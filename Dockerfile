FROM rustlang/rust:nightly

RUN apt-get update && apt-get install -y qemu qemu-system-x86
RUN rustup component add rust-src
RUN rustup component add llvm-tools-preview
RUN cargo install bootimage

WORKDIR /usr/src/blog_os
COPY . .
RUN cargo install --path .

#CMD tail -f /dev/null
CMD cargo test
